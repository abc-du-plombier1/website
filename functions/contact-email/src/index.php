<?php

require(__DIR__ . '/../vendor/autoload.php');

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Swoole\Runtime;

Runtime::setHookFlags(SWOOLE_HOOK_ALL ^ SWOOLE_HOOK_TCP);

return function ($context) {
  if ($context->req->method !== 'POST' || $context->req->headers['content-type'] !== 'application/x-www-form-urlencoded') {
    return $context->res->send('Not found', 404);
  }

  $errorMessage = '';
  $codeError = 200;
  $data = json_decode($context->req->body, true);
  $variables = array(
    "SMTP_USERNAME" => getenv('SMTP_USERNAME'),
    "SMTP_PASSWORD" => getenv('SMTP_PASSWORD'),
    "SMTP_AUTH" => getenv('SMTP_AUTH') === "true",
    "SMTP_PORT" => (int)getenv('SMTP_PORT'),
    "SMTP_HOST" => getenv('SMTP_HOST')
  );

  if ($data['name'] == null) {
    $errorMessage = 'Name is required';
    $codeError = 400;
  }

  if ($data['firstname'] == null) {
    $errorMessage = 'Firstname is required';
    $codeError = 400;
  }

  if ($data['phoneNumber'] == null) {
    $errorMessage = 'Phone number is required';
    $codeError = 400;
  }

  if ($data['email'] == null) {
    $errorMessage = 'Email is required';
    $codeError = 400;
  }

  if ($data['message'] == null) {
    $errorMessage = 'Message is required';
    $codeError = 400;
  }

  if ($codeError != 200) {
    return $context->res->send($errorMessage, $codeError);
  }


  $status = sendEmail($data, $variables, $context);
  if ($status != 0) {
    return $context->res->send('An error occurred.', 500);
  }

  return $context->res->send('Success', $codeError);
};

function sendEmail($data, $env, $context)
{
  include 'mail-template.php';

  $body = contactFormTemplate($data);

  $mail = new PHPMailer();  // Create new PHPMailer
  try {
    $mail->isHTML(true);
    $mail->Encoding = 'base64';
    $mail->CharSet = 'UTF-8';
    $mail->IsSMTP(); // enable SMTP
    $mail->SMTPDebug = 1;  // debug: 1 = errors and message, 2 = message only
    $mail->SMTPAuth = true;  // Enable SMTP authenticate
    $mail->Username = $env['SMTP_USERNAME']; // SMTP username
    $mail->Password = $env['SMTP_PASSWORD'];
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Host = "pro2.mail.ovh.net";
    $mail->Port = 587;
    $mail->SetFrom($env['SMTP_USERNAME'], $env['SMTP_USERNAME']);
    $mail->Subject = 'ABC du Plombier';
    $mail->Body = $body;
    $mail->AddAddress($data['email']);
    $mail->AddAddress($env['SMTP_USERNAME']);
    if ($mail->Send()) {
      return 0;
    }
    return 1;
  } catch (Exception $e) {
    return $e;
  }
}
