<?php

function contactFormTemplate($data)
{

//Mail content
  //Header
  $bodyContactForm = '<header style="width=100px; background-color:white; padding-bottom:10px;  text-align: center;">';
  $bodyContactForm .= '<img src="https://abcduplombier44.fr/assets/icon/abc_big_logo.png" width="200" height="200" alt="logo ABC du Plombier">';
  $bodyContactForm .= '<h2 style="display:block" >ABC DU PLOMBIER</h2>';
  $bodyContactForm .= '</header>';

  //Main
  $bodyContactForm .= '<div style=" background-color:#4F98CF; color:white; padding-left:20px; padding-top:10px; padding-bottom:20px; " >';
  $bodyContactForm .= '<h3>Bonjour Madame/Monsieur ' . $data['name'] . ' ' . $data['firstname'] . ',</h3>';
  $bodyContactForm .= '<br> <h6>Votre numéro de téléphone: ' . $data['phoneNumber'] . ' </h6>';
  $bodyContactForm .= '<br> <p>Nous avons bien reçu votre message ! Nous nous chargeons de vous répondre au plus vite ! <br> <p>Votre message :</p>' . $data['message'] . '</p>';
  if ($data['quotationMessage']) {
    $bodyContactForm .= '<br>';
    $bodyContactForm .= '<h6>Type de devis : ' . $data['quotationMessage'] . '</h6>';
  }
  $bodyContactForm .= '</div>';

  //footer
  $bodyContactForm .= '<footer style="text-align: center;" >';
  $bodyContactForm .= '<p>06 40 37 96 23 <br> projets@abcduplombier44.fr <br> <a style="text-decoration: none; color:black;" href="https://abcduplombier44.fr/"><h3>ABC du Plombier</h3></a> </p>';
  $bodyContactForm .= '</footer>';

  return $bodyContactForm;
}

?>
