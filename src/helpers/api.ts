import {Client, Databases, Functions, Storage} from 'appwrite';
import {environment} from '../environments/environment';

export class Api {
  private static sdk: Client | null;
  private static database: Databases;
  private static function: Functions;
  private static storage: Storage;

  static client() {
    if (this.sdk) {
      return this.sdk;
    }

    const client = new Client();
    client
      .setEndpoint(environment.serverEndpoint)
      .setProject(environment.projectId)
      .setLocale('fr-FR');
    this.sdk = client;

    return this.sdk;
  }

  static databases(): Databases {
    if (!this.sdk) {
      this.client();
    }
    if (this.database) {
      return this.database;
    }

    this.database = new Databases(this.sdk!);

    return this.database;
  }

  static functions(): Functions {
    if (!this.sdk) {
      this.client();
    }
    if (this.function) {
      return this.function;
    }

    this.function = new Functions(this.sdk!);

    return this.function;
  }

  static storages(): Storage {
    if (!this.sdk) {
      this.client();
    }
    if (this.storage) {
      return this.storage;
    }

    this.storage = new Storage(this.sdk!);

    return this.storage;
  }
}
