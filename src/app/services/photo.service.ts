import {Injectable} from '@angular/core';
import {Api} from '../../helpers/api';
import {PhotosModel} from '../shared/photos.model';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class PhotoService {

  bucketID = '62b8b55ed810d77117de';
  collectionID = '62b8b64047898200d4ed';
  listFiles: any = [];
  photosObject: PhotosModel[] = [];

  getPhotos(): PhotosModel[] {
    Api.storages().listFiles(this.bucketID).then(
      res => {
        this.listFiles.push(...res.files);
        for (const photo of this.listFiles) { // Loop throw photos
          const result = Api.storages().getFileView(this.bucketID, photo.$id);
          Api.databases().getDocument(environment.databaseId, this.collectionID, photo.$id).then((res: any) => { // Get comment associate to the photo
            this.photosObject.push(new PhotosModel(photo.$id, result.href, res.description));
          });
        }
      }
    );
    return this.photosObject;
  }
}
