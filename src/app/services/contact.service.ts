import {ContactForm} from '../contact/contact-dialog/contact-dialog.component';
import {Injectable} from '@angular/core';
import {Api} from '../../helpers/api'

@Injectable({providedIn: 'root'})
export class ContactService {

  functionID = '6530f3920d90a24bcf8b'

  /**
   * Send a message by email.
   * @param data data needed to send the email
   */
  sendMessage(data: ContactForm) {
    console.log(JSON.stringify(data))
    return Api.functions().createExecution(
      this.functionID,
      JSON.stringify(data),
      false);
  }
}
