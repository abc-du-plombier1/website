import {Injectable} from '@angular/core';
import {Api} from '../../helpers/api';
import {environment} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class ReviewsService {

  collectionID = '62c965f36986d0a4f33b'
  reviews: any;

  getReviews() {
    return Api.databases()
      .listDocuments(environment.databaseId, this.collectionID)
      .then((reviews: any) => {
        return reviews;
      });
  }
}
