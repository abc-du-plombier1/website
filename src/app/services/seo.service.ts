import {Meta, Title} from '@angular/platform-browser';
import {Inject, Injectable} from '@angular/core';
import {DOCUMENT} from '@angular/common';

export interface TagConfig {
  title: string,
  description: string
}

@Injectable({providedIn: 'root'})
export class SeoService {

  link: HTMLLinkElement = this.dom.createElement('link');
  baseUrl = 'https://abcduplombier44.fr'

  constructor
  (
    private readonly metaTagService: Meta,
    private readonly titleService: Title,
    @Inject(DOCUMENT) private dom: any
  ) {
  }


  /**
   * Generate meta tag dynamically
   * @param config
   */
  generateTags(config: TagConfig) {
    this.titleService.setTitle('ABC du Plombier - ' + config.title);
    this.metaTagService.updateTag({name: 'description', content: config.description});
  }

  /**
   * Create canonical link
   * @param url
   */
  createCanonicalLink(url?: string) {
    let canURL = url == undefined ? this.dom.URL : url;
    this.link.setAttribute('rel', 'canonical');
    this.dom.head.appendChild(this.link);
    this.link.setAttribute('href', this.baseUrl + canURL);
  }

  /**
   * Update canonical link dynamically
   * @param canURL
   */
  updateCanonicalLink(canURL: string) {
    this.link.setAttribute('href', this.baseUrl + canURL);
    // console.log(location.href);
    this.dom.getElementsByTagName('base')[0].href = canURL;
  }
}
