import {Injectable} from '@angular/core';
import {Api} from '../../helpers/api';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class QuotationService {

  collectionID = '62b8b4eb3874ceb6e158';

  /**
   * Get the list of actions available.
   */
  getActions() {
    return Api.databases().listDocuments(environment.databaseId, this.collectionID);
  }
}
