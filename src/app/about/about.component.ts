import {Component, OnInit} from '@angular/core';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  isReady = false;
  values = [
    {
      title: 'Responsabiliser les autres',
      text: 'Nous consacrons beaucoup de notre temps au travail, et il y a une raison importante pour laquelle nous le faisons tous. Nous sommes au service des autres, et notre travail donne du pouvoir aux personnes qui le vivent. Nous prenons plaisir à rendre les gens plus heureux.'
    },
    {
      title: 'Adoptez l\'authenticité',
      text: 'Les meilleures entreprises sont construites sur la base d\'une communication forte et authentique. Nous n\'achetons pas de fans, nous ne spammons pas notre message. Nous enseignons, nous partageons, nous écoutons.'
    },
    {
      title: 'Construire ensemble',
      text: 'Nous nous soucions de la personne qui se trouve de l\'autre côté de tout ce que nous créons. Nous prenons le temps de comprendre qui elle est, et comment elle peut voir le monde. Cela nous permet de créer de meilleurs produits qui résolvent des problèmes réels.'
    },
    {
      title: 'L\'équipe d\'abbord',
      text: 'Chacun chez ABC du Plombier joue un rôle d\'une importance unique ; nous n\'avons pas de place pour les rockstars. Nous sommes un groupe respectueux, collaboratif et solidaire, et notre impact est maximisé par la confiance et la composition du travail.'
    },
    {
      title: 'Voir grand',
      text: 'Nous sommes hyper concentrés sur le fait de faire mieux, et d\'être meilleurs, dans chaque chose que nous faisons. Nous voulons être reconnus pour cela. Si nous ne savons pas quelque chose, nous l\'apprenons. Si nous avons besoin d\'aide, nous la demandons. Notre plus grand concurrent, c\'est nous-mêmes.'
    }
  ]

  constructor(private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'Qui sommes nous',
      description: 'À propos de ABC du Plombier'
    });
    this.isReady = true;
  }

}
