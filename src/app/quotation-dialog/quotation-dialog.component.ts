import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {QuotationService} from '../services/quotation.service';
import {MatDialog} from '@angular/material/dialog';
import {ContactDialogComponent} from '../contact/contact-dialog/contact-dialog.component';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-quotation-contact-dialog',
  templateUrl: './quotation-dialog.component.html',
  styleUrls: ['./quotation-dialog.component.css']
})
export class QuotationDialogComponent implements OnInit {

  constructor(private quotationService: QuotationService,
              private dialog: MatDialog,
              @Inject(PLATFORM_ID) private platformId: any) {
  }

  quotations: any = [];
  serviceSelected: string = '';

  ngOnInit(): void {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.quotationService.getActions().then((res: any) => {
      if (res.documents) {
        this.quotations = res.documents;
        console.log(this.quotations)
      }
    }, err => {
      console.log(err);
    });
  }

  getPrice(serviceName: string) {
    for (let item of this.quotations) {
      if (item.name === serviceName) {
        return Number(item.price);
      }
    }
    return;
  }

  onAsked() {
    console.log(this.serviceSelected);
    this.dialog.open(ContactDialogComponent, {
      data: {
        quotationMessage: this.serviceSelected
      }
    });
  }
}
