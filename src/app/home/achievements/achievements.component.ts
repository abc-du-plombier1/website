import {AfterViewInit, Component, Inject, PLATFORM_ID} from '@angular/core';
import {PhotoService} from '../../services/photo.service';
import {PhotosModel} from '../../shared/photos.model';
import {SwiperOptions} from 'swiper/types';


@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.css']
})
export class AchievementsComponent implements AfterViewInit {

  config: SwiperOptions = {
    effect: 'coverflow',
    coverflowEffect: {
      rotate: 25,
      stretch: 0,
      depth: 50,
      modifier: 1,
      slideShadows: false
    },
    loop: true,
    navigation: false,
    grabCursor: true,
    spaceBetween: 90,
    autoplay: {
      delay: 2500,
      disableOnInteraction: true
    },
    breakpoints: {
      300: {
        slidesPerView: 1,
      },
      1024: {
        slidesPerView: 3,
      }
    }
  }

  photos: PhotosModel[] = [];

  constructor(private photoService: PhotoService,
              @Inject(PLATFORM_ID) private platformId: any) {
  }

  ngAfterViewInit() {
    // if (!isPlatformBrowser(this.platformId)) {
    //   return;
    // }
    // this.photos = this.photoService.getPhotos();
  }
}
