import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnInit,
  PLATFORM_ID,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {ReviewsService} from '../../services/reviews.service';
import {isPlatformBrowser} from '@angular/common';
import {SwiperOptions} from 'swiper/types';


export interface Review {
  author_name: string;
  profile_photo_url: string;
  rating: number;
  text: string;
}

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ReviewsComponent implements OnInit, AfterViewInit {
  @ViewChild('swiper', {static: true}) swiperRef: ElementRef | undefined;
  reviews: Review[] = [];
  // config: SwiperOptions = {
  //   navigation: true,
  //   autoplay: {
  //     delay: 2500,
  //     disableOnInteraction: true
  //   }
  // }
  config: SwiperOptions = {
    loop: true,
    centeredSlides: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: true,
      pauseOnMouseEnter: true
    }
  }

  constructor(private reviewsService: ReviewsService,
              @Inject(PLATFORM_ID) private platformId: any) {
  }

  ngOnInit(): void {
    this.fetchReviews();
  }

  ngAfterViewInit() {
    Object.assign(this.swiperRef?.nativeElement, this.config);
  }

  /**swiperSlide
   * Fetch reviews from Google.
   */
  fetchReviews() {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    this.reviewsService.getReviews().then(
      (reviews) => {
        this.reviews = reviews.documents;
      });
  }

  stars(starNumber: number) {
    return Array(starNumber);
  }
}
