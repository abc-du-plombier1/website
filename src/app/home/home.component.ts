import {Component, OnInit} from '@angular/core';

import {ContactDialogComponent} from '../contact/contact-dialog/contact-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog: MatDialog, private seoService: SeoService) {
  }

  actionItem = [
    {
      title: 'Dépannage & Réparation',
      subText: [
        "Chasse d'eau",
        "Robinetterie cuisine et salle de bain",
        "Tuyaux d'évacuation",
        "Douche et baignoire",
        "Chauffe-eau"
      ],
      icon: '../assets/icon/home_repair_service.webp',
      color: '#FDF0D6',
      alt: 'Logo plomberie réparation'
    },
    {
      title: 'Installation Sanitaire',
      subText: [
        "Installation de matériel de cuisine, SdB (évier, lavabo, douche, baignoire, ...) et wc",
        "Installation de réseau d'eau potable et évacuation"
      ],
      icon: '../assets/icon/bathtub.webp',
      color: '#DDF2FD',
      alt: 'Logo salle de bain'
    },
    {
      title: 'Installation Thermique',
      subText: [
        "Sèche-serviette à eau chaude",
        "Radiateur",
        "Chauffe-eau électrique",
        "Pompe à chaleur Air/Eau",
        "Chaudière gaz à condensation"
      ],
      icon: '../assets/icon/local_fire.webp',
      color: '#FDE7EF',
      alt: 'Logo flamme thermique'
    }];

  workProcess = [
    {
      title: '1. DIAGNOSTIC ET DEVIS GRATUITS',
      text: 'Que ce soit pour un dépannage urgent, pour étudier un projet de rénovation de vos installations ou pour la création complète d’une salle de bain ou de votre cuisine, nous nous déplaçons gratuitement pour étudier votre cahier des charges et évaluer ensemble le type d’intervention la plus pertinente en vous apportant les meilleures solutions techniques.'
    },
    {
      title: '2. RAPIDITÉ D’INTERVENTION',
      text: 'Nous mettons tout en œuvre pour vous contacter rapidement afin d’intervenir dans les meilleurs délais sur la région nantaise dans un rayon de 30 kms autour de Nantes Métropole'
    },
    {
      title: '3. PRESTATIONS DE QUALITÉ',
      text: 'Nous travaillons avec des marques de référence afin de nous adapter à vos souhaits esthétiques et vos contraintes de budget. Notre large gamme de produits nous permet de répondre à vos besoins d’un point de vue technique en fonction des utilisations souhaitées. En tant qu’artisan RGE et membre de la capeb, la qualité du travail bien fait est notre priorité.'
    },
    {
      title: '4. GARANTIES',
      text: 'Notre assurance décennale et nos nombreuses attestations de formation (certifications et labellisations) démontrent notre sérieux et la valeur des services que nous délivrons.'
    },
  ]

  badges = [
    {
      name: 'Logo Artisan',
      imagePath: '../assets/images/badge/logo-artisan.webp'
    },
    {
      name: 'Logo Capeb',
      imagePath: '../assets/images/badge/logo-capeb.webp'
    },
    {
      name: 'Logo Chauffage',
      imagePath: '../assets/images/badge/logo-chauffage.webp'
    },
    {
      name: 'Logo Qualitenr',
      imagePath: '../assets/images/badge/logo-qualitenr.webp'
    },
    {
      name: 'Logo QualiPAC',
      imagePath: '../assets/images/badge/QualiPAC_Formation.webp'
    }
  ]

  isReady = false;

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'NANTES',
      description: 'ABC du Plombier est une entreprise Rezéenne spécialisée dans la plomberie et la pose de chaudière gaz. Nous rayonnons autour de l’agglomération nantaise. Appelez le 06 40 37 96 23 pour un devis gratuit !'
    });
    this.isReady = true;

  }

  onContact() {
    this.dialog.open(ContactDialogComponent);
  }

}
