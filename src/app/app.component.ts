import {AfterViewInit, Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {SeoService} from './services/seo.service';
import {isPlatformBrowser} from '@angular/common';
import {register} from 'swiper/element/bundle';
import {filter} from 'rxjs/operators';

declare let AOS: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  constructor(
    private seoService: SeoService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit() {
    register();

    this.seoService.createCanonicalLink();
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    ).subscribe((val: any) => {
      this.seoService.updateCanonicalLink(val.url)
    })
  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      AOS.init({
        once: true,
        duration: 800
      });
    }
  }
}
