import {Component, OnInit} from '@angular/core';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-heating',
  templateUrl: './heating.component.html',
  styleUrls: ['./heating.component.css']
})
export class HeatingComponent implements OnInit {
  isReady = false;

  actionItem = [
    {
      title: 'RÉPARATION CHAUDIERE',
      subText: 'Ne laissez pas votre système de chauffage vous laisser dans le froid cet hiver. ABC du Plombier vous aidera lorsque vous devez faire réparer votre chaudière.',
      icon: '../assets/icon/boiler-repair.png',
    },
    {
      title: 'INSTALLATION CHAUDIERE',
      subText: 'Nous ne nous contentons pas de réparer et d\'entretenir les chaudières, nous proposons également une installation de chaudière de première qualité.',
      icon: '../assets/icon/boiler-installation.png',
    },
    {
      title: 'MAINTENANCE CHAUDIERE',
      subText: 'Vous espérez éviter les problèmes de chaudière onéreux? Vous voulez réduire les coûts de chauffage sans sacrifier le confort ? Des mises au point régulières peuvent rendre votre système plus efficace.',
      icon: '../assets/icon/boiler-settings.png',
    }];

  constructor(private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'Chauffage',
      description: 'ABC du Plombier fournit des services de haute qualité dans la région de Nantes. Nous offrons une garantie de satisfaction à 100 %. Appelez-nous ou demandez votre devis en ligne !'
    });
    this.isReady = true;
  }

}
