import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-faq-heat',
  templateUrl: './faq-heat-pump.component.html',
  styleUrls: ['./faq-heat-pump.component.css'],
})
export class FaqHeatPumpComponent implements OnInit {

  accordionItems =
    [
      {
        title: 'Quels sont les services offerts par ABC du Plombier en matière de pompes à chaleur ?',
        text: 'ABC du Plombier prend en charge uniquement l\'installation de pompe à chaleur.'
      },
      // {
      //   title: 'À quelle fréquence dois-je faire entretenir ma pompe à chaleur ?',
      //   text: 'Il est recommandé de faire entretenir votre pompe à chaleur au moins une fois par an, de préférence avant le début de la saison de chauffage et de refroidissement. Un entretien régulier permet de s\'assurer que votre pompe à chaleur fonctionne de manière sûre et efficace.'
      // },
      // {
      //   title: 'Quels sont les signes indiquant que ma pompe à chaleur a besoin d\'être réparée ?',
      //   text: 'Parmi les signes les plus courants indiquant que votre pompe à chaleur a besoin d\'être réparée, citons une baisse des performances de chauffage ou de refroidissement, des bruits étranges provenant de l\'unité, une accumulation de glace sur l\'unité extérieure et des factures d\'énergie plus élevées. Si vous remarquez l\'un de ces signes, il est préférable de prévoir un service de réparation avec ABC du Plombier dès que possible.'
      // },
      {
        title: 'ABC du Plombier peut-il remplacer ma pompe à chaleur si elle est irréparable ?',
        text: 'Oui, ABC du Plombier peut vous aider à choisir et à installer une nouvelle pompe à chaleur si votre appareil actuel est irréparable. Notre équipe peut vous guider tout au long du processus de sélection et s\'assurer que vous choisissez une pompe à chaleur qui répond à vos besoins de chauffage et de refroidissement et à votre budget.'
      },
      // {
      //   title: 'Combien coûte l\'entretien d\'une pompes à chaleur par ABC du Plombier ?',
      //   text: 'Le coût d\'un entretien de pompe à chaleur peut varier en fonction du type d\'entretien nécessaire, de la taille de votre pompe à chaleur et d\'autres facteurs. Contactez ABC du Plombier pour obtenir un devis personnalisé en fonction de vos besoins spécifiques.'
      // },
      {
        title: 'Comment puis-je planifier l\'entretien de ma pompe à chaleur avec ABC du Plombier ?',
        text: 'Vous pouvez prendre rendez-vous avec ABC du Plombier pour l\'entretien de votre pompe à chaleur en nous contactant par le biais de notre site Web ou par téléphone. Notre équipe travaillera avec vous pour trouver un moment opportun pour votre rendez-vous d\'entretien.'
      },
    ]

  constructor() {
  }

  ngOnInit(): void {
  }

}
