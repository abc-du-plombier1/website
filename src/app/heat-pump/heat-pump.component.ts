import {Component, OnInit} from '@angular/core';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-heat-pump',
  templateUrl: './heat-pump.component.html',
  styleUrl: './heat-pump.component.scss'
})
export class HeatPumpComponent implements OnInit {
  isReady = false;

  actionItem = [
    {
      title: 'Réparation pompe à chaleur',
      subText: 'Votre pompe à chaleur ne fonctionne plus correctement ? Notre équipe de spécialistes est à votre disposition pour diagnostiquer et réparer rapidement tout type de panne. Nous utilisons des pièces de haute qualité pour garantir une réparation durable et efficace.',
      icon: '../assets/icon/pompe-a-chaleur-blue.png',
    },
    {
      title: 'Installation pompe à chaleur',
      subText: 'Vous envisagez d\'installer une pompe à chaleur pour un chauffage plus économique et écologique ? Nos experts vous accompagnent de A à Z, depuis le choix de l\'équipement adapté jusqu\'à son installation professionnelle, pour un confort optimal en toute saison.',
      icon: '../assets/icon/pompe-a-chaleur-orange.png',
    },
    {
      title: 'Maintenance pompe à chaleur',
      subText: 'Assurez la longévité et l\'efficacité de votre pompe à chaleur avec notre service de maintenance. Nous proposons des contrats d\'entretien réguliers pour prévenir les pannes et optimiser les performances de votre système, tout en réduisant vos coûts énergétiques.',
      icon: '../assets/icon/pompe-a-chaleur-red.png',
    }];

  makeDifferent =
    [
      {
        title: 'La pompe à chaleur ne démarre pas',
        subtitle: 'C\'est le premier signe évident que votre pompe à chaleur est en panne. Si votre pompe à chaleur ne se met pas en marche ou ne chauffe pas, c\'est qu\'elle est endommagée et qu\'il est temps d\'appeler l\'un de nos techniciens en réparation de pompe à chaleur pour une réparation rapide.',
      },
      {
        title: 'Bruit excessif ou sons étranges',
        subtitle: 'Votre pompe à chaleur ne devrait pas être bruyante. En effet, des bruits différents indiquent des problèmes différents que vous ne pourrez peut-être pas diagnostiquer vous-même. Si votre pompe à chaleur produit des bruits étranges après l\'avoir mise en marche, il est temps de prendre rendez-vous avec l\'un de nos techniciens spécialisés dans la réparation de pompes à chaleur.',
      },
      {
        title: 'Cyclisme court',
        subtitle: 'Si votre pompe à chaleur se met en marche et s\'arrête trop fréquemment, il est temps d\'appeler un professionnel de la réparation de pompes à chaleur. Les cycles courts nuisent à l\'efficacité de votre pompe à chaleur et l\'usent beaucoup plus rapidement, ce qui nécessite un remplacement coûteux plus tôt.',
      },
      {
        title: 'Augmentation sans précédent des factures d\'électricité',
        subtitle: 'Une pompe à chaleur qui fonctionne mal est inefficace pour rafraîchir votre maison.Si vous constatez que vos factures de chauffage ou de climatisation augmentent sans que le fonctionnement de l\'équipement ait changé, c\'est le signe que la pompe à chaleur ne fonctionne pas correctement. Planifiez une réparation de la pompe à chaleur dès que possible.',
      }, {
      title: 'Faible débit d\'air',
      subtitle: 'Lorsque l\'accumulation de glace, de poussière ou de débris bloque le flux d\'air de la pompe à chaleur, votre maison sera probablement inconfortable, car la pompe a besoin d\'un flux d\'air pour maintenir votre maison et votre bureau à une température stable. Lorsque cela se produit, il est temps de prévoir une inspection et une réparation de la pompe à chaleur.',
    }, {
      title: 'Glace sur l\'unité extérieure',
      subtitle: 'La formation de glace sur l\'unité extérieure lorsque la pompe à chaleur chauffe le bureau est normale, mais une couche épaisse et excessive de glace indique une restriction du flux d\'air.',
    }, {
      title: 'Refroidissement ou chauffage inversé',
      subtitle: 'Si votre pompe à chaleur produit de l\'air froid lorsque vous voulez chauffer votre maison ou votre bureau, ou inversement, c\'est le signe qu\'elle a des problèmes et qu\'elle doit être réparée.',
    }, {
      title: 'Réduction du refroidissement ou du chauffage',
      subtitle: 'Devez-vous faire fonctionner la pompe plus longtemps qu\'auparavant pour refroidir ou chauffer votre maison ou votre bureau ? Si oui, la pompe à chaleur fonctionne mal et il est temps d\'appeler un professionnel pour la réparer.',
    },
    ]


  constructor(private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'Installation pompe à chaleur',
      description: 'ABC du Plombier fournit des services de haute qualité dans la région de Nantes. Nous offrons une garantie de satisfaction à 100 %. Appelez-nous ou demandez votre devis en ligne !'
    });
    this.isReady = true;
  }

}
