import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {QuotationDialogComponent} from '../quotation-dialog/quotation-dialog.component';

interface NavigationSections {
  sectionName: string;
  links: {
    text: string,
    hrefAttribute?: string,
    routerAttribute?: string
  }[]
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  year: string = new Date().getFullYear().toString();

  sections: NavigationSections[] = [
    {
      sectionName: 'Contact',
      links: [
        {
          text: '24 rue Aristide Nogues, Rezé - 44400',
          hrefAttribute: 'https://www.qwant.com/maps/place/addr:-1.538438;47.180175:24@24_Rue_Aristide_Nogues#map=16.50/47.1801750/-1.5384380',
        },
        {
          text: '06 40 37 96 23',
          hrefAttribute: 'tel:+33640379623'
        },
        {
          text: 'projets@abcduplombier44.fr',
          hrefAttribute: 'mailto:projets@abcduplombier44.fr'
        },
        {
          text: 'abcduplombier44.fr',
          hrefAttribute: 'https://abcduplombier44.fr'
        }
      ]
    },

    {
      sectionName: 'Information',
      links: [
        {
          text: 'Mentions légales',
          routerAttribute: '/mentions-legales'
        },
        {
          text: 'Avis Google',
          hrefAttribute: 'https://www.google.fr/search?q=abc+du+plombier'
        },
        {
          text: 'Dépôt Gitlab',
          hrefAttribute: 'https://gitlab.com/abc-du-plombier1'
        }
      ]
    },

    {
      sectionName: 'Navigation',
      links: [
        {
          text: 'Accueil',
          routerAttribute: '/'
        },
        {
          text: 'Salle de Bain',
          routerAttribute: '/salle-de-bain'
        },
        {
          text: 'Chauffage',
          routerAttribute: '/chauffage'
        },
        {
          text: 'Qui sommes nous ?',
          routerAttribute: '/qui-sommes-nous'
        }
      ]
    }
  ]

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  onPresentQuotationDialog() {
    this.dialog.open(QuotationDialogComponent);
  }

  protected readonly Date = Date;
}
