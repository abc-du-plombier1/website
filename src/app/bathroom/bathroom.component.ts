import {Component, OnInit} from '@angular/core';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-bathroom',
  templateUrl: './bathroom.component.html',
  styleUrls: ['./bathroom.component.css']
})
export class BathroomComponent implements OnInit {

  isReady = false;

  makeDifferent =
    [
      {
        title: 'PRIX & CERTIFICATIONS',
        subtitle: 'Nous sommes titulaires de prix et de certifications dans le secteur',
        iconPath: '../assets/icon/medal.png',
        iconName: 'logo medaille'
      },
      {
        title: 'SOURCES FIABLES',
        subtitle: 'Nos concepteurs utilisent des matériaux de haute qualité provenant de fournisseurs de confiance.',
        iconPath: '../assets/icon/trust.png',
        iconName: 'logo de confiance'
      },
      {
        title: 'RESPECTUEUX DE L\'ENVIRONNEMENT',
        subtitle: 'La plupart de nos matériaux sont respectueux de l’environnement.',
        iconPath: '../assets/icon/eco-friendly.png',
        iconName: 'logo de eco responsable'
      },
      {
        title: 'VENTES SANS PRESSION',
        subtitle: 'Nous voulons que vous soyez confiant dans votre décision, pas de vente sous pression.',
        iconPath: '../assets/icon/saving.png',
        iconName: 'logo de cochon argent'
      }
    ]

  constructor(private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'Salle de Bain',
      description: 'Vous envisagez de rénover la salle de bain de votre maison à Nantes ? ABC du Plombier est un spécialiste de la rénovation des salles de bains de Nantes. Appelez-nous.'
    });

    this.isReady = true;
  }

}
