import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  accordionItems =
    [
      {
        title: ' Quels sont les éléments nécessaires à la rénovation d\'une salle de bain ? ',
        text: 'Mettez tout en œuvre pour réussir la rénovation de votre salle de bains. Travaillez avec ABC du Plombier, une entreprise professionnelle de rénovation de salles de bains à Nantes. S\'il s\'agit d\'une nouvelle salle de bain, il est important d\'obtenir les permis nécessaires. Puisque vous travaillez généralement avec un espace fixe pour la rénovation de votre salle de bains, il est essentiel d\'avoir une équipe de conception de premier ordre pour assurer un maximum de rangement, de fonctionnalité et de joie dans votre nouvelle salle de bains. Que vous souhaitiez ajouter une baignoire autonome ou transformer une ancienne baignoire/douche en une douche à effet pluie équipée des dernières technologies, c\'est le moment d\'améliorer votre structure, votre plomberie et votre électricité. Toute rénovation de salle de bains commence par la démolition. La démolition d\'une salle de bains est un travail salissant. Laissez cela aux entrepreneurs de salle de bains !!! Vos entrepreneurs de rénovation de salle de bains à Nantes sont formés pour vérifier et éliminer les moisissures, un problème courant dans les vieilles salles de bains.'
      },
      {
        title: ' Que se passe-t-il pendant la rénovation d\'une salle de bain ?',
        text: 'Une fois la conception de votre nouvelle salle de bain finalisée, votre équipe d\'entrepreneurs en rénovation de salle de bain terminera la démolition. En fonction de l\'ampleur de la rénovation de la salle de bains, votre équipe se mettra au travail pour effectuer les travaux nécessaires de plomberie, d\'électricité et d\'amélioration de la structure. C\'est à vous qu\'incombe la tâche amusante d\'acheter et de confirmer vos choix en matière de nouvel éclairage, de revêtement de sol, d\'accessoires de baignoire et de douche, de toilettes et de vanité. Il y a tellement de nouveaux choix de produits modernisés avec des technologies de pointe, des fonctions d\'économie d\'eau et des matériaux respectueux de l\'environnement. Passez à des toilettes à faible débit d\'eau. ABC du Plombier peut personnaliser les vanités et les rangements afin de maximiser l\'espace et de garder votre salle de bains sans encombrement.'
      },
      {
        title: ' Combien coûte la rénovation d\'une salle de bains à Nantes ? ',
        text: 'Beaucoup de nos clients vous diront qu\'ils ont choisi ABC du Plombier comme entreprise de rénovation de salle de bain pour nos prix ainsi que pour notre équipe de conception, notre savoir-faire et la facilité avec laquelle il est possible de communiquer et de travailler avec nous. Nous respectons les délais et le budget, à chaque fois. Le prix d\'un ajout de salle de bain ou d\'une rénovation de salle de bain est basé sur la conception et l\'étendue du projet. Quelle est l\'ampleur des travaux à réaliser ? Vous souhaitez peut-être simplement remplacer votre vanité et votre revêtement de sol. Ou votre projet peut consister à ajouter la salle de bain principale dont vous rêvez depuis des années. Nos équipes se rendront chez vous et vous fournira un devis gratuit. Cela comprend l\'examen du site, la prise des mesures nécessaires et la collaboration avec vous pour comprendre vos souhaits et vos idées de conception. Il n\'y a pas de coûts cachés. Une fois que nous aurons finalisé le plan, vous connaîtrez les coûts, l\'étendue des travaux et le calendrier d\'exécution. Appelez-nous dès aujourd\'hui pour commencer à concevoir la salle de bains de vos rêves. Laissez-nous vous surprendre avec nos tarifs abordables et notre financement facile.'
      },
    ]

  constructor() {
  }

  ngOnInit(): void {
  }

}
