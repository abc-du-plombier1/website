import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LegalNoticeComponent} from './legal-notice/legal-notice.component';
import {AboutComponent} from './about/about.component';
import {BathroomComponent} from './bathroom/bathroom.component';
import {HeatingComponent} from './heating/heating.component';
import {HeatPumpComponent} from './heat-pump/heat-pump.component';

const routes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path: 'mentions-legales', component: LegalNoticeComponent},
  {path: 'qui-sommes-nous', component: AboutComponent},
  {path: 'salle-de-bain', component: BathroomComponent},
  {path: 'chauffage', component: HeatingComponent},
  {path: 'pompe-a-chaleur', component: HeatPumpComponent},
  {path: '**', pathMatch: 'full', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
