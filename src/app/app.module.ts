import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {MatInputModule} from '@angular/material/input';
import {AppRoutingModule} from './app-routing.module';
import {AboutComponent} from './about/about.component';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {ContactComponent} from './contact/contact.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BathroomComponent} from './bathroom/bathroom.component';
import {SnackbarComponent} from './snackbar/snackbar.component';
import {ReviewsComponent} from './home/reviews/reviews.component';
import {LegalNoticeComponent} from './legal-notice/legal-notice.component';
import {LazyLoadImagesDirective} from './shared/lazy-load-images.directive';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule, provideClientHydration} from '@angular/platform-browser';
import {AchievementsComponent} from './home/achievements/achievements.component';
import {ContactDialogComponent} from './contact/contact-dialog/contact-dialog.component';
import {QuotationDialogComponent} from './quotation-dialog/quotation-dialog.component';
import {provideHttpClient, withFetch, withInterceptorsFromDi} from '@angular/common/http';
import {FaqComponent} from './bathroom/faq/faq.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {HeatingComponent} from './heating/heating.component';
import {HeatPumpComponent} from './heat-pump/heat-pump.component';
import {FaqHeatPumpComponent} from './heat-pump/faq-heat-pump/faq-heat-pump.component';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ReviewsComponent,
    FooterComponent,
    LegalNoticeComponent,
    AboutComponent,
    LazyLoadImagesDirective,
    ContactComponent,
    ContactDialogComponent,
    BathroomComponent,
    QuotationDialogComponent,
    SnackbarComponent,
    AchievementsComponent,
    FaqComponent,
    HeatingComponent,
    HeatPumpComponent,
    FaqHeatPumpComponent
  ],
  bootstrap: [AppComponent],
  imports: [BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatSnackBarModule,
    MatListModule,
    ReactiveFormsModule,
    FormsModule,
    MatExpansionModule],
  providers: [provideClientHydration(), provideHttpClient(withFetch()), provideHttpClient(withInterceptorsFromDi())]
})
export class AppModule {
}
