import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ContactDialogComponent} from './contact-dialog/contact-dialog.component';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openContactDialog() {
    this.dialog.open(ContactDialogComponent);
  }
}
