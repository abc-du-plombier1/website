import {ContactService} from '../../services/contact.service';
import {SnackBarService} from '../../services/snack-bar.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export interface ContactForm {
  name: string,
  firstname: string,
  phoneNumber: string,
  email: string,
  message: string,
  quotationMessage: string | null;
}

@Component({
  selector: 'app-contact-dialog',
  templateUrl: './contact-dialog.component.html',
  styleUrls: ['./contact-dialog.component.css']
})
export class ContactDialogComponent implements OnInit, OnDestroy {

  form: FormGroup = new FormGroup({
    name: new FormControl(null, Validators.required),
    firstname: new FormControl(null, Validators.required),
    phoneNumber: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.email]),
    message: new FormControl(null, Validators.required)
  });
  loading = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { quotationMessage: string },
    private dialogRef: MatDialogRef<ContactDialogComponent>,
    private contactService: ContactService,
    private snackService: SnackBarService) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    this.loading = true;
    const formValue = this.form.value;
    const data: ContactForm = {
      name: formValue.name,
      firstname: formValue.firstname,
      phoneNumber: formValue.phoneNumber,
      email: formValue.email,
      message: formValue.message,
      quotationMessage: this.data ? this.data.quotationMessage : null
    }
    this.contactService.sendMessage(data).then(
      res => {
        let message = '';
        if (res.responseStatusCode === 200) {
          message = 'Mesage envoyé avec succès !'
          this.dialogRef.close();
        } else {
          this.loading = false;
          console.error(res)
          message = 'Erreur: ' + res.errors + ' ' + res.responseStatusCode;
        }
        this.snackService.showMessage(message);
      },
      err => {
        console.error(err);
        this.loading = false;
        this.snackService.showMessage('Une erreur c\'est produite');
      })
  }

  ngOnDestroy() {
    this.loading = false;
  }
}
